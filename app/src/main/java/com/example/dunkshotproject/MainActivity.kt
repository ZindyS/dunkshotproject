package com.example.dunkshotproject

import android.annotation.SuppressLint
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Float.max
import java.lang.Float.min
import java.lang.Math.cos
import java.lang.Math.pow
import kotlin.math.*

//Класс описания экрана с игрой
class MainActivity : AppCompatActivity() {
    var start = false
    var start2 = true
    var start3 = false
    var positionX = 0f
    var positionY = 0f
    var dunk_nextX = 120f
    var dunk_nextY = 1000f

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            //Начальная анимация
        //Задержка
        Handler().postDelayed({}, 400)

        //Подготовка анимации
        newDunk(600f, 700f)
        animationDisappear(dunk1)
        animationDisappear(dunk2)
        animationDisappear(dunknext1)
        animationDisappear(dunknext2)
        animationDisappear(ball)
        animationDisappear(score)



        //Анимация лого
        logo.animate()
            .setDuration(1500)
            .alpha(1f)
            .withEndAction {
                //Вторая анимация лого
                logo.animate()
                    .setDuration(750)
                    .translationY(-700f)
                    .scaleX(0.7f)
                    .scaleY(0.7f)
                    .withEndAction {
                        //Анимация появления
                        animationDunk(dunknext2)
                        animationDunk(dunknext1)
                        animationDunk(dunk1)
                        animationDunk(dunk2)
                        animationDunk(ball)

                        start = true
                    }
                    .start()
            }
            .start()

            //Игра
        //Обработка нажатий
        surface.setOnTouchListener { _, event ->
            //Если можно начинать играть
            if (start) {
                var angle=0.0
                var lx = max(min(positionX-event.x, 1000f), -1000f)
                var ly = max(min(positionY-event.y, 1000f), -1000f)
                //Проверка на то, какое действие совершается
                if (event.action == MotionEvent.ACTION_DOWN) {
                    //Проверка на то, ушёл ли логотип
                    if (start2) {
                        //Убираем логотип
                        logo.animate()
                            .setDuration(500)
                            .alpha(0f)
                            .translationYBy(-300f)
                                .withEndAction {
                                    start2 = false
                                    start3 = true
                                }
                            .start()

                        //Передвижение всех элементов экрана (кроме счётчика и логотипа)
                        animationMove(background, 200f)
                        animationMove(ball, 1400f)
                        animationMove(dunk1, 1200f)
                        animationMove(dunk2, 1200f)
                        animationMove(dunknext1, 200f+dunk_nextY)
                        animationMove(dunknext2, 200f+dunk_nextY)

                        //Появление счётчика
                        animationDunk(score)
                    }

                    //Запоминаем координаты
                    positionX = event.x
                    positionY = event.y
                } else if (start3) {
                    if (event.action == MotionEvent.ACTION_MOVE) {
                        //Если "оттягивание" достаточно сильное, то нужно построить траекторию
                        if (abs(lx) > 300 || abs(ly) > 300) {
                            traject1.visibility=View.VISIBLE
                            traject2.visibility=View.VISIBLE
                            traject3.visibility=View.VISIBLE
                            traject4.visibility=View.VISIBLE
                            traject5.visibility=View.VISIBLE
                            traject6.visibility=View.VISIBLE
                            traject7.visibility=View.VISIBLE
                            traject8.visibility=View.VISIBLE

                            //Показываем траекторию
                            writeTrajectory(traject1, 2f, lx, ly)
                            writeTrajectory(traject2, 3f, lx, ly)
                            writeTrajectory(traject3, 4f, lx, ly)
                            writeTrajectory(traject4, 5f, lx, ly)
                            writeTrajectory(traject5, 6f, lx, ly)
                            writeTrajectory(traject6, 7f, lx, ly)
                            writeTrajectory(traject7, 8f, lx, ly)
                            writeTrajectory(traject8, 9f, lx, ly)
                        } else {
                            //Убираем траекторию
                            traject1.visibility = View.INVISIBLE
                            traject2.visibility = View.INVISIBLE
                            traject3.visibility = View.INVISIBLE
                            traject4.visibility = View.INVISIBLE
                            traject5.visibility = View.INVISIBLE
                            traject6.visibility = View.INVISIBLE
                            traject7.visibility = View.INVISIBLE
                            traject8.visibility = View.INVISIBLE
                        }

                        //Подсчёт на то, на сколько нужно передвинуть корзину, чтобы всё смотрелось гармонично
                        var resultX = min(max(lx / abs(lx) * (-max(0f, abs(lx) - 100) / 9), -20f), 20f) + dunk1.x
                        var resultY = min(max(ly / abs(ly) * (-max(0f, abs(ly) - 100)) / 9, -20f), 20f) + dunk1.y
                        if (resultX.isNaN()) { resultX = dunk1.x }
                        if (resultY.isNaN()) { resultY = dunk1.y }

                        //Подсчёт угла поворота
                        angle = 180 / PI * (acos(lx / (sqrt(pow(lx.toDouble(), 2.0) + pow(ly.toDouble(), 2.0)))))

                        if (!angle.isNaN()) {
                            if (positionY < event.y) {
                                angle = 360 - angle
                            }
                            angle += 90

                            //Поворот корзины и мяча
                            dunk1.animate()
                                    .setDuration(0)
                                    .rotation(angle.toFloat())
                                    .start()
                            dunk2.animate()
                                    .setDuration(0)
                                    .rotation(angle.toFloat())
                                    .scaleY(1 + max(min(sqrt(pow(lx.toDouble(), 2.0) + pow(ly.toDouble(), 2.0)).toFloat(), 500f), 0f) / 1000)
                                    .translationY(resultY)
                                    .translationX(resultX)
                                    .start()
                            ball.animate()
                                    .setDuration(0)
                                    .translationX((resultX - 20) * 2)
                                    .translationY( (resultY - 500) * 2)
                                    .start()
                        }
                    } else if (event.action == MotionEvent.ACTION_UP) { //Если палец убрали с экрана
                        //Убираем траекторию
                        traject1.visibility = View.INVISIBLE
                        traject2.visibility = View.INVISIBLE
                        traject3.visibility = View.INVISIBLE
                        traject4.visibility = View.INVISIBLE
                        traject5.visibility = View.INVISIBLE
                        traject6.visibility = View.INVISIBLE
                        traject7.visibility = View.INVISIBLE
                        traject8.visibility = View.INVISIBLE

                        //Проверка на то, что "оттянули" достаточно для броска
                        if (abs(lx) > 300 || abs(ly) > 300) {
                            //Останавливаем возможность управлять корзиной
                            start3=false
                            //Анимация выброска из корзины
                            dunk2.animate()
                                    .setDuration(50)
                                    .scaleY(0.8f)
                                    .translationY(dunk1.y + (ly + lx * lx / abs(lx)) / (abs(ly) + abs(lx)) * 20)
                                    .translationX(dunk1.x)
                                    .withEndAction {
                                        dunk2.animate()
                                                .setDuration(100)
                                                .scaleY(1f)
                                                .start()
                                    }
                                    .start()
                            //Проверка на то, куда направлен "бросок" (вверх или вниз)
                            var time = abs(ly).toLong()
                            if (ly<0) {
                                //Анимация подбрасывания мячика
                                ball.animate()
                                        .setDuration(time)
                                        .translationY(ly+dunk1.y)
                                        .setInterpolator(LinearOutSlowInInterpolator())
                                        .withEndAction {
                                            //Анимация падения
                                            animationDown(ball, time)
                                        }
                                        .start()
                            } else {
                                //Анимация падения
                                animationDown(ball, ly.toLong()/5)
                                time = max((abs(ly)/5).toLong(), 100.toLong())
                            }
                            //Анимация движения мячика влево или вправо
                            ball.animate()
                                    .setDuration(time*3)
                                    .translationX(lx*2)
                                    .rotationBy(5000f)
                                    .setInterpolator(LinearInterpolator())
                                    .withEndAction {
                                        //Возвращаем мячик обратно
                                        ball.animate()
                                                .setDuration(0)
                                                .translationY(dunk1.y+100)
                                                .translationX(dunk1.x+80)
                                                .withEndAction {
                                                    ball.animate()
                                                            .setDuration(200)
                                                            .translationY(dunk1.y+200)
                                                            .setInterpolator(AccelerateInterpolator(1.5f))
                                                            .withEndAction {
                                                                start3=true
                                                            }
                                                            .start()
                                                }
                                                .start()

                                    }
                                    .start()
                        } else { //Если "оттянули" недостаточно сильно
                            dunk2.animate()
                                    .setDuration(100)
                                    .scaleY(1f)
                                    .translationY(dunk1.y)
                                    .translationX(dunk1.x)
                                    .start()
                            ball.animate()
                                    .setDuration(100)
                                    .translationY(dunk1.y + 200)
                                    .translationX(dunk1.x + 80)
                                    .start()
                        }
                    }
                }

            }
            return@setOnTouchListener true
        }
    }

    //Анимация появления
    fun animationDunk(dunk: View) {
        dunk.animate()
            .setDuration(300)
            .alpha(1f)
            .scaleX(1.2f)
            .scaleY(1.2f)
            .setInterpolator(AccelerateInterpolator(0.2f))
            .withEndAction {
                dunk.animate()
                        .setDuration(100)
                        .scaleX(1f)
                        .scaleY(1f)
                        .setInterpolator(AccelerateInterpolator(1.5f))
                        .start()
            }
            .start()
    }

    //Подготовительная "анимация", чтобы убрать предметы с экрана
    fun animationDisappear(dunk: View) {
        dunk.animate()
            .setDuration(0)
            .scaleX(0f)
            .scaleY(0f)
            .start()
    }

    //Анимация передвижения предметов
    fun animationMove(dunk: View, trans: Float) {
        dunk.animate()
            .setDuration(300)
            .translationY(trans)
            .setInterpolator(LinearInterpolator())
            .start()
    }

    //Анимация падения мячика
    fun animationDown(dunk: View, ly: Long) {
        dunk.animate()
                .setDuration(max((abs(ly)), 200.toLong()))
                .translationYBy(abs(ly)+1000f+dunk1.y)
                .setInterpolator(AccelerateInterpolator(1.2f))
                .start()
    }

    //Функция для отображения траектории
    fun writeTrajectory(tr: View, time: Float, lx: Float, ly: Float) {
        tr.animate()
                .setDuration(0)
                .translationY(dunk1.y+(-ly/4.5f-9.8f*time)*-time)
                .translationX(dunk1.x+lx*time/2)
                .start()
    }

    fun newDunk(x: Float, y: Float) {
        dunk1.animate()
                .setDuration(0)
                .translationY(dunk_nextY)
                .translationX(dunk_nextX)
                .start()

        dunk2.animate()
                .setDuration(0)
                .translationY(dunk_nextY)
                .translationX(dunk_nextX)
                .start()

        dunknext1.animate()
                .setDuration(0)
                .translationY(y)
                .translationX(x)
                .start()

        dunknext2.animate()
                .setDuration(0)
                .translationY(y)
                .translationX(x)
                .start()

        ball.animate()
                .setDuration(100)
                .translationY(dunk_nextY+200)
                .translationX(dunk_nextX+80)
                .start()

        dunk_nextX=x
        dunk_nextY=y
    }
}